---
layout: default
id: du-signalement-au-mandat
title: Du signalement au mandat
image: /img/scene_1.jpg
---
Suite à un signalement (qui peut être fait par la personne concernée, sa famille, son réseau médico-social, etc), le Tribunal de protection de l’adulte instruit le dossier et décide d’instaurer ou non une mesure.

Le Tribunal choisit le curateur selon différents critères, dont celui de la situation pécuniaire de la personne concernée. La limite communément admise à Genève pour qu’un mandat soit confié à un mandataire privé est de CHF 50'000.--. Néanmoins, si les revenus réguliers du protégé sont suffisamment élevés pour assumer une prise en charge privée ou si la famille est disposée à assumer les honoraires du curateur, le Tribunal peut nommer un mandataire privé.

Les tâches du curateur sont généralement les suivantes :

*   établissement d’un budget
*   gestion des revenus
*   remise d’argent
*   paiements des frais
*   gestion des frais médicaux
*   investissements financiers
*   assistance personnelle : suivi social à domicile et organisation du quotidien, travail en réseau avec la famille et les professionnels (infirmiers, éducateurs, employés, etc) entourant le protégé, etc.

L’activité du curateur est contrôlée par le Tribunal auquel des rapports sont rendus (inventaire des biens après deux mois d’entrée en fonction puis rapport tous les deux ans).

Toute demande de relève, de changement de mesure ou de mandataire doit être adressée au Tribunal qui est l’unique autorité compétente.