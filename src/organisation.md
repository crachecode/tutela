---
layout: default
id: organisation
title: Organisation
image: /img/classeurs.jpg
---
Le Tribunal de protection de l’adulte nomme Bérénice Alberti ou Loyse Hamoir en qualité de curatrice qui est alors la personne de référence pour son protégé et est entièrement responsable du dossier.

Néanmoins, nous nous informons mutuellement et de manière régulière de nos situations respectives et sommes attentives à être disponible en cas d’absence de l’une ou de l’autre.

Nos honoraires sont contrôlés par le Tribunal de protection. Le tarif était de CHF 150.-/heure jusqu'au 31 décembre 2021. Depuis le 1er janvier 2022, le Tribunal a aligné les tarifs des curateurs privés, nous sommes autorisés à facturer CHF 120.-/heure.