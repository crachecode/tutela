---
layout: default
id: berenice-alberti
title: Bérénice Alberti
image: /img/berenice_1.jpg
---
Mastère en Psychologie clinique et pathologique de l’Université de Nice en 2000

Service de protection de l’adulte de 2001 à 2009

Tutela depuis 2009

Juge assesseur au Tribunal de Protection de l’Adulte depuis 2013

Elle anime des conférences sur le droit et la pratique des mesures de protection.

Elle fait partie du groupe de travail de deux grands projets de refonte de la curatelle à Genève : 

1. projet de Gestion de mandataires au TPAE - Pouvoir Judiciaire 
2. le projet Projet RePAir –  Repenser la protection de l'adulte pour l'avenir du DCS