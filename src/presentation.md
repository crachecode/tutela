---
layout: default
id: presentation
title: Présentation
image: /img/cours_int.jpg
---
Tutela est un bureau d’exécution de mandats de protection privés créé en 2009.

Tutela était le premier bureau à Genève exécutant exclusivement des mandats de protection.

Que la complexité du dossier se situe au niveau social, médical, administratif ou financier, nos compétences nous permettent d’assurer un suivi de qualité.

Grâce à notre expérience, nous pouvons nous appuyer sur un réseau de proximité solide et varié : médecins, soins à domicile, avocats, notaires, déménageurs, entreprises de nettoyage et menus travaux à domicile, etc. Ce réseau, que nous avons construit, complète de manière personnalisée le réseau médico-social genevois usuel.

Nous suivons tant des personnes souffrant de troubles psychiques ou mentaux que des personnes âgées à domicile ou en EMS.

Nous nous tenons à la disposition de toute personne ayant des questions et/ou besoin d'aide pour effectuer un signalement afin de donner un conseil en amont d'une demande de mandat au Tribunal de protection de l’adulte.