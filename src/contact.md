---
layout: default
id: contact
title: Contact
---
<section>
	<p>
		<b>Bérénice Alberti</b><br>
		T +41 78 326 30 04<br>
		<a href="mailto:berenice.alberti@tutela.ch">berenice.alberti@tutela.ch</a>
	</p>
	<p>
		<b>Loyse Hamoir</b><br>
		T +41 79 371 16 10<br>
		<a href="mailto:loyse.hamoir@tutela.ch">loyse.hamoir@tutela.ch</a>
	</p>
</section>
<section>
	<p>
		2, rue Patru<br>
		1205 Genève
	</p>
	<p class="smaller">
		Case postale 524<br>
		1211 Genève 4
	</p>
	{% comment %}
		<p>
			F +41 22 328 02 85
		</p>
	{% endcomment %}
</section>
<section>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2761.713286720569!2d6.13813441560392!3d46.1962638923174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c7ad56ec5daa9%3A0xb577dd6b73157806!2sRue+Patru+2%2C+1205+Gen%C3%A8ve!5e0!3m2!1sfr!2sch!4v1482023029793" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>