---
layout: default
id: nouveau-droit
title: Nouveau droit
image: /img/codecivil.jpg
---
Le Tribunal tutélaire se nomme désormais Tribunal de protection de l’adulte et de l’enfant. Il fonctionne en collégialité : un juge de carrière (président) et deux juges assesseurs (psychiatre et travailleur social/psychologue).

Le nouveau droit est entré en vigueur le 1er janvier 2013 et est défini aux articles 360 ss du Code civil.

Les mesures de curatelle sont les suivantes :

*   Curatelle d’accompagnement : le curateur assiste la personne concernée en lui prodiguant conseils, aide, mises en contact et encouragements.
*   Curatelle de représentation avec gestion : le curateur représente son protégé dans les tâches faisant l’objet du mandat.
*   Curatelle de coopération : certains actes, préalablement définis par le Tribunal, doivent faire l’objet d’une autorisation ou d’une ratification par le curateur.
*   Curatelle de portée générale : elle remplace la mesure de tutelle et est instituée lorsque la personne concernée a besoin d’une aide prononcée en raison d’une incapacité durable de discernement. Elle couvre tous les domaines de l’assistance personnelle, de la gestion du patrimoine et des rapports juridiques.

Il existe des restrictions qui peuvent compléter les mesures de curatelle de représentation avec gestion.

*   Restriction de l’exercice des droits civils (la signature de la personne concernée n’est plus valable)
*   Blocage des comptes
*   Représentation dans le domaine médical (ne s’applique qu’en cas de perte de discernement)

Il ressort de la pratique que ce sont essentiellement les mesures de curatelle de représentation avec gestion et de curatelle de portée générale qui sont instituées.

Enfin, certaines mesures de contraintes peuvent être prononcées par le TPAE en cas de risque vital :

*   La mesure de placement à des fins d’assistance (PAFA), placement contre le gré de la personne concernée dans un établissement hospitalier ou médico-social.
*   L’obligation d’un suivi thérapeutique ambulatoire