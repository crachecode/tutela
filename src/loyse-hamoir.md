---
layout: default
id: loyse-hamoir
title: Loyse Hamoir
image: /img/loyse_1.jpg
---
Mastère en Psychologie générale et pédagogique, Psychologie clinique et Travail social à l’Université de Fribourg en 2000

Service de protection de l’adulte de 2001 à 2003 puis de 2007 à 2009, Service de protection des mineurs en 2004 et 2005

Tutela depuis 2009
