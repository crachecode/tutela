# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du contenu éditorial du site tutela.ch

basé sur [Jekyll](https://jekyllrb.com), [Netlify CMS](https://www.netlifycms.org) et [GitLab](https://gitlab.com).

## utilisation

- front-end : https://www.tutela.ch/
- gestion du contenu : https://www.tutela.ch/admin/

## gestion des accès

L'accès à la gestion du contenu se fait au moyen d'un compte [GitLab](https://gitlab.com/users/sign_in).  
L'utilisateur doit avoir au minimum un rôle _Maintainer_ sur le projet [tutela](https://gitlab.com/crachecode/tutela/-/project_members) ou le groupe [crachecode](https://gitlab.com/groups/crachecode/-/group_members).
